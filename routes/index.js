const express = require("express");
const router = express.Router();
const siscotRouter = require("./siscot");

router.use("/siscoti", siscotRouter);

module.exports = router;
