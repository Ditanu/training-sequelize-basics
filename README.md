### Tema pentru croco smecheri Sequelize:
##### NodeJS + Sequelize:

Fie baza noastra de date si schema ei cea de toate zilele:
![schema](https://imagizer.imageshack.com/img924/3991/WqSU7d.jpg)

``Nota: Pentru BD se vor folosi functiile Sequelize-ului(ex. functia create pentru insert) , nu comenzi de SQL``
**
``Aici aveti link-ul cu documentatia Sequelize-ului:`` https://sequelize.org/master/manual/model-instances.html

1. Pe baza notiunilor invatate la training, adaugati o ruta de tip POST ce va insera un SiSCot in baza de date.
``Indicatie: Modul de adaugare este la fel ca cel prezentat la training: Definiti functia in folderul controllers in user.js -> Definiti ruta POST in folderul routes si apelati functia definita in controllers pe ruta respectiva. ***
PS: Aveti grija la departmentId ;)
``
 ``` 
Exemplu apel in Postman: POST http://localhost:8080/addSiscot 
```

``Merge? Bravo clan, trecem in etapa urmatoare!``

2. Pe baza notiunilor invatate la training, realizati inca o ruta de tip POST ce va adauga un SiSCot pe un proiect.
``
Indicatie: Daca tabela "siscots" si tabela "proiects" sunt legate prin tabela "proiectesiscoti", ganditi-va in ce tabela se va face insert-ul.
``
 ``` 
Exemplu apel in Postman: POST http://localhost:8080/addSiscotOnProiect
```
`` PS: Notiunile pe care le veti folosi la aceasta tema includ si notiunile invatate la primul training(back-end basics) asa ca nu uitati sa mai aruncati o privire si pe acela in caz ca ati uitat ceva. ``


### Felicitari! Esti la un pas mai aproape de un back-end croco veritabil.
![gifObosit2](https://media.giphy.com/media/3oEhn4A5PQmhnYD3t6/giphy.gif)

***PS: Google is your friend***
***Pentru orice fel de intrebare suntem la dispozitia voastra la un mesaj distanta(Nu va fie teama sa intrebati indiferent de problema). Daca vi se pare greu, e ok, e chiar greu. Dar totul se poate invata si suntem aici sa va raspundem la intrebari si sa va ajutam la orice. Important este sa incercati si sa fiti perseverenti pentru ca suntem siguri ca va va iesi schema***.

