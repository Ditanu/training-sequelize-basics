module.exports = (sequelize, DataTypes) => {
  return sequelize.define("proiect", {
    nume: DataTypes.STRING(20),
    dataInceput: DataTypes.DATE,
    dataFinal: DataTypes.DATE,
  });
};
