module.exports = (sequelize, DataTypes) => {
  return sequelize.define("departament", {
    nume: DataTypes.STRING(20),
    numarMembri: DataTypes.INTEGER,
  });
};
