module.exports = (sequelize, DataTypes) => {
  return sequelize.define("siscot", {
    nume: DataTypes.STRING(20),
    prenume: DataTypes.STRING(20),
    varsta: DataTypes.INTEGER,
    email: DataTypes.STRING(30),
    telefon: DataTypes.STRING(10),
  });
};
