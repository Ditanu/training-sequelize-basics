const Sequelize = require("sequelize");
const db = require("../config/db");
const SiscotModel = require("./siscot");
const ProiectModel = require("./proiect");
const DepartamentModel = require("./departament");

const Siscot = SiscotModel(db, Sequelize);
const Proiect = ProiectModel(db, Sequelize);
const Departament = DepartamentModel(db, Sequelize);
Departament.hasMany(Siscot, { foreignKey: "departamentID" });
Siscot.belongsTo(Departament, { foreignKey: "departamentID" });
Siscot.belongsToMany(Proiect, {
  through: "ProiecteSiscoti",
});
Proiect.belongsToMany(Siscot, {
  through: "ProiecteSiscoti",
});

module.exports = {
  Departament,
  Proiect,
  Siscot,
  connection: db,
};
